# MIT License
#
# Copyright (c) 2020 Björn Richter
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Usage:
#   declare -A <var-name>=([key1]="val1", [key2]="val2", ...)
#   load-config [--no-strict] <var-name> <config-file>
#
# OPTIONS:
#   --no-strict
#               Do not strictly test if the config key is valid. Validation is
#               done by testing if the key is already declared in the config
#               variable.
#
# EXAMPLE:
#   This will load a <config-file> into an associative array of name <var-name>.
#   The only two config keys that are allowed are `leet` and `path`
# 
#      declare -A myConf=(
#          [leet]='1337'
#          # Path has no default value but must be declared as valid key.
#          [path]=''
#          [is-awesome]=1
#      )
#      load-config myConf /path/to/config.conf
#      echo "${myConf[is-awesome]}"
#
#   This will load a <config-file> in non-strict mode. This means that not all
#   valid keys must be declared in the config array beforeh calling the
#   `load-config` method:
#
#     declare -A myOtherConfig
#     load-config --no-strict myOtherConfig /path/to/other-config.conf
#     echo "${myOtherConfig[is-also-awesome]}"
#
load-config() {
    # Envorinment variables
    local DEBUG="${LOAD_CONFIG_DEBUG:-0}"

    # Options
    local strict=1
    if [[ $1 == '--no-strict' ]]; then
        shift
        strict=0
    fi

    # Arguments
    local var_name="$1"
    local config_file="$2"

    local FNAME="${FUNCNAME[0]}"

    if (( DEBUG )); then
        echo >&2 "${FNAME}: [DEBUG] Called ${FNAME}"
        echo >&2 "${FNAME}: [DEBUG]     (\$1) Config variable name: ${var_name}"
        echo >&2 "${FNAME}: [DEBUG]     (\$2) Config file: ${config_file}"
    fi

    if [[ -z $var_name ]]; then
        echo >&2 "ERROR: ${FNAME}: The config variable name must not be empty"
        return 1
    fi
    if [[ ! $var_name =~ ^[a-zA-Z0-9_.-]+$ ]]; then
        echo >&2 "ERROR: ${FNAME}: The config variable name \"${var_name}\" is not a valid variable name"
        return 1
    fi
    if ! declare -p "$var_name" >/dev/null 2>&1; then
        echo >&2 "ERROR: ${FNAME}: The variable \"${var_name}\" is not declared. Please declare it with \`declare -A ${var_name}\`"
        return 1
    fi
    if [[ ! -f $config_file ]]; then
        echo >&2 "ERROR: ${FNAME}: The config file \"${config_file}\" does not exist"
        return 1
    fi

    declare -n __load_cofig_ref__="$var_name"

    local line_num=0
    while read -r line; do
        if (( DEBUG )); then
            echo >&2 "${FNAME}: [DEBUG] ($line)"
        fi

        let line_num++

        if [[ $line =~ ^$|^\# ]]; then
            continue
        fi

        # sq-str:  single-quote string
        # dq-str:  double-quote string
        # bool:    true / false
        # numeric: integer / float (no leading zeroes allowd)
        #                                                           ( 2 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - )
        #                                                              ( 3: sq-str - )     ( 5: dq-str - )   ( 7: bool - - - - ) ( 8: numeric - - - - - - - - -)
        #                 ( 1: key - - - -)            =                ( 4 - - - -)        ( 6 - - - -)                          ( 9 - - - - - )( 10 - - )
        if [[ ! $line =~ ^([a-zA-Z0-9_.-]+)[[:space:]]*=[[:space:]]*(\'(([^\']|\\\')*)\'|\"(([^\"]|\\\")*)\"|(true|false|yes|no)|((0|[1-9][0-9]*)(\.[0-9]+){0,1}))$ ]]; then
            echo >&2 "ERROR: ${FNAME}: Error parsing config in line ${line_num}: ${config_file}:${line_num}"
            return 1
        fi

        local key="${BASH_REMATCH[1]}"
        local string_single_quote="${BASH_REMATCH[3]//\\\'/\'}"
        local string_double_quote="${BASH_REMATCH[5]//\\\"/\"}"
        local boolean_value="${BASH_REMATCH[7]}"
        local boolean_value="${boolean_value/true/1}"
        local boolean_value="${boolean_value/false/0}"
        local boolean_value="${boolean_value/yes/1}"
        local boolean_value="${boolean_value/no/0}"
        local numeric_value="${BASH_REMATCH[8]}"

        local value="${string_single_quote}${string_double_quote}${boolean_value}${numeric_value}"

        if (( strict )) && [[ ! -v __load_cofig_ref__[$key] ]]; then
            echo >&2 "ERROR: ${FNAME}: Invalid config key \"${key}\" in: ${config_file}:${line_num}"
            return 1
        fi

        __load_cofig_ref__[$key]="$value"

        if (( DEBUG )); then
            local i=0
            for x in "${BASH_REMATCH[@]}"; do
                echo >&2 "${FNAME}: [DEBUG]     ${i}: |$x|"
                let i++
            done
        fi
    done < "$config_file"
}

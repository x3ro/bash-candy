#!/usr/bin/env bash

SRC_DIR="$(dirname "${BASH_SOURCE%/*}")"

source "${SRC_DIR}/load-config.bash"



# -----------------------------------------------------------------------------
# Load values from a config (it is important to declare an associative array
# variable with all valid keys beforehand):

#LOAD_CONFIG_DEBUG=1

declare -A CONFIG=(
    [Hello]="Default"
    [hello.text]=
    [hello.text2]=
    [is-awesome]=
    [true]=
    [false]=
    [pi]=3.1
    [THE_ANSWER]=
    [empty]=
)

load-config CONFIG "${SRC_DIR}/example.conf"


echo "Hello, ${CONFIG[Hello]}!"
echo "${CONFIG[hello.text]}"
echo "${CONFIG[hello.text2]}"
echo -n "Awesome: "
if (( ${CONFIG[is-awesome]} )); then
    echo "YES :)"
else
    echo "NO :("
fi
echo "true: ${CONFIG[true]}"
echo "false: ${CONFIG[false]}"
echo "Emptiness is: ${CONFIG[empty]}"



# -----------------------------------------------------------------------------
# In non-strict mode, it is also possible to just specify default values (keep
# in mind though, that a config value might actually not set):

declare -A CONFIG_WITH_DEFAULTS=(
    [default]="default"
)
load-config --no-strict CONFIG_WITH_DEFAULTS "${SRC_DIR}/example.conf"


echo "Default values are: ${CONFIG_WITH_DEFAULTS[default]}"
echo "PI: ${CONFIG[pi]}"



# -----------------------------------------------------------------------------
# To simply rely on the config file without having any default values, this is
# sufficient (but still: some values might not be set):

declare -A myConfig
load-config --no-strict myConfig "${SRC_DIR}/example.conf"

echo "The Answer to the Ultimate Question of Life, the Universe, and Everything: ${myConfig[THE_ANSWER]}"